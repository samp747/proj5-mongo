"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    control_dist_km = round(control_dist_km)
    # 3 cases/types of checkpoints:
    # Start checkpoint: When control_dist_km == 0. Opens at brevet start time
    if control_dist_km == 0:
        time = brevet_start_time
    # Intermediate checkpoint: When 0 < control_dist < brevet_dist_km
    elif 0<control_dist_km<brevet_dist_km:
        print(f"Control: {control_dist_km}")
        time_segments =  [(200, 34), (400, 32), (600, 30), (1000, 28)]
        time = arrow.get(brevet_start_time)
        remaining_dist = control_dist_km
        prev = 0
        for segment_dist, speed in time_segments:
            if remaining_dist:
                dist = segment_dist-prev
                dist = min(dist, remaining_dist)
                segment_time = dist/speed
                hours = segment_time//1
                minutes = round((segment_time%1)*60)
                time = time.shift(hours=hours)
                time = time.shift(minutes=minutes)
                remaining_dist -= dist
                prev = segment_dist
        time = time.isoformat()
    # Finish checkpoint: When control_dist>=brevet_dist_km
    elif control_dist_km >= brevet_dist_km:
        print(f"Control: {control_dist_km}")
        time_segments =  [(200, 34), (400, 32), (600, 30), (1000, 28)]
        time = arrow.get(brevet_start_time)
        remaining_dist = brevet_dist_km
        prev = 0
        for segment_dist, speed in time_segments:
            if remaining_dist:
                dist = segment_dist-prev
                dist = min(dist, remaining_dist)
                segment_time = dist/speed
                hours = segment_time//1
                minutes = round((segment_time%1)*60)
                time = time.shift(hours=hours)
                time = time.shift(minutes=minutes)
                remaining_dist -= dist
                prev = segment_dist
        time = time.isoformat()
    return time



def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    time_segments =  [(200, 15), (400, 15), (600, 15), (1000, 11.428)]
    control_dist_km = round(control_dist_km)
    # 3 cases/types of checkpoints:
    # Start checkpoint: When control_dist_km == 0. Opens an hour after brevet start time
    if control_dist_km == 0:
        time = arrow.get(brevet_start_time)
        time = time.shift(hours=1)
        time = time.isoformat()
    # Intermediate checkpoint: When 0 < control_dist < brevet_dist_km
    elif 0<control_dist_km<brevet_dist_km:
        print(f"Control: {control_dist_km}")
        time = arrow.get(brevet_start_time)
        remaining_dist = control_dist_km
        prev = 0
        for segment_dist, speed in time_segments:
            if remaining_dist:
                dist = segment_dist-prev
                dist = min(dist, remaining_dist)
                segment_time = dist/speed
                hours = segment_time//1
                minutes = round((segment_time%1)*60)
                time = time.shift(hours=hours)
                time = time.shift(minutes=minutes)
                remaining_dist -= dist
                prev = segment_dist
        time = time.isoformat()
    # Finish checkpoint: When control_dist>=brevet_dist_km
    elif control_dist_km >= brevet_dist_km:
        finish_times = {200: (13, 30), 300: (20, 0), 400: (27, 0), 600: (40, 0), 1000: (75, 0)}
        time = arrow.get(brevet_start_time)
        max_time = finish_times[brevet_dist_km]
        time = time.shift(hours=max_time[0])
        time = time.shift(minutes=max_time[1])
        time = time.isoformat()
    return time
